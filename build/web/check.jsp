<%-- 
    Document   : check
    Created on : Apr 28, 2011, 6:43:40 AM
    Author     : Thilok
--%>
<%@page import="hibernateutilities.HibernateUtil"%>
<%@page import="org.hibernate.criterion.Projections"%>
<%@page import="org.hibernate.criterion.MatchMode"%>
<%@page import="org.hibernate.criterion.Order"%>
<%@page import="org.hibernate.FetchMode"%>
<%@page import="persistanceclasses.Logger"%>
<%@page import="persistanceclasses.Specialist"%>
<%@page import="persistanceclasses.Problem"%>
<%@page import="org.hibernate.SessionFactory"%>
<%@page import="org.hibernate.criterion.Restrictions"%>
<%@page import="persistanceclasses.Operator"%>
<%@page import="persistanceclasses.Caller"%>
<%@page import="persistanceclasses.LoginValid2"%>
<%@page import="persistanceclasses.Employee2"%>
<%@page import="persistanceclasses.EmployeeSalary"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="persistanceclasses.LoginValid"%>
<%@page import="persistanceclasses.Employee"%>
<%@page import="org.hibernate.Session"%>
<%@page import="org.hibernate.Transaction"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
        <%@ taglib prefix="hiber" uri="http://java.hibertags.com/hibertags_1.0"%>
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>





        <%          Session ss = HibernateUtil.getSessionFactory().openSession();
                    List list = ss.createCriteria(persistanceclasses.Logger.class).add(Restrictions.ne("problemDescription", "Sound Card Error")).list();
                    request.setAttribute("attribute", list);
        %>

        <hiber:from className="persistanceclasses.Logger" var="w">
            <hiber:notEq propertyName="problemDescription" value="Sound Card Error" type="string"/>
        </hiber:from>

        <hiber:join className="persistanceclasses.Logger" var="v" associationPath="specialist" alias="a">
            <hiber:alias associationPath="problem" alias="p"/>
            <hiber:fetchMode associationPath="p" fetchMode="join"/>
            <hiber:greaterThan propertyName="a.noOfProblems" value="8" type="int"/>
            <hiber:greaterThan propertyName="p.problemType" value="10" type="int"/>
        </hiber:join>

        <hiber:from className="persistanceclasses.Specialist" var="u">


        </hiber:from>

        <hiber:rowCount className="persistanceclasses.Logger" var="a"/>
        <hiber:rowCount className="persistanceclasses.Specialist" var="b"/>
        <hiber:rowCount className="persistanceclasses.Problem" var="c"/>
        <c:out value="${a}"/>
        <c:out value="${b}"/>
        <c:out value="${c}"/>
        <c:out value="${d}"/>

        <table border="1">
            <tr>
                <td>
                    <c:out value="Criteria API Query Results"/>
                </td>
                <td>
                    <c:out value="Hibertags Results"/>
                </td>
                <td>
                    <c:out value="Whatever"/>
                </td>
            </tr>

            <tr>
                <td>
                    <table>
                        <c:forEach var="emp2" items="${v}">
                            <tr>
                                <td><c:out value="${emp2.problemDescription}"/></td>
                                <td><c:out value="${emp2.problem.problemType}"/></td>
                                <td><c:out value="${emp2.specialist.spId}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
                <td>
                    <table>
                        <c:forEach var="emp" items="${u}">
                            <tr>
                                <td><c:out value="${emp.noOfProblems}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
                <td>
                    <table>
                        <c:forEach var="emp3" items="${w}">
                            <tr>

                                <td><c:out value="${emp3.problem.problemType}"/></td>
                                <td><c:out value="${emp3.specialist.spId}"/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </table>












        <%
                    /*
                    Transaction tx = null;
                    Session s = HibernateUtil.getSessionFactory().openSession();
                    tx = s.beginTransaction();
                    Employee2 e = new Employee2("Sahan Aluthge", "Accounts Department", "Accountant");
                    LoginValid2 lv = new LoginValid2("sahan", "222", "director", e);
                    Caller cl = new Caller(e);
                    Operator op = new Operator(e);
                    Problem prob = new Problem(12);
                    Specialist sp=new Specialist(5, prob, e);
                    Logger lg=new Logger("router problem", cl, prob, sp,op);
                    s.save(e);
                    s.save(lv);
                    s.save(cl);
                    s.save(op);
                    s.save(prob);
                    s.save(sp);
                    s.save(lg);
                    tx.commit();
                    s.flush();
                    s.close();
                     */

        %>
    </body>
</html>
