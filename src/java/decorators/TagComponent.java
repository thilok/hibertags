/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators;

import java.util.List;
import hibernateutilities.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;

/**
 *
 * @author Thilok
 */
public abstract class TagComponent {

    public Session s = HibernateUtil.getSessionFactory().openSession();
    public DetachedCriteria dc;
    public List list;

    public abstract void execute();

    /**
     * @return the s
     */
    public Session getS() {
        return s;
    }

    /**
     * @param s the s to set
     */
    public void setS(Session s) {
        this.s = s;
    }

    /**
     * @return the dc
     */
    public DetachedCriteria getDc() {
        return dc;
    }

    /**
     * @param dc the dc to set
     */
    public void setDc(DetachedCriteria dc) {
        this.dc = dc;
    }

    /**
     * @return the list
     */
    public List getList() {
        return list;
    }

    /**
     * @param list the list to set
     */
    public void setList(List list) {
        this.list = list;
    }
}
