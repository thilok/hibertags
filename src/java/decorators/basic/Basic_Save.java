/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package decorators.basic;

import decorators.TagComponent;
import org.hibernate.Session;
import org.hibernate.Transaction;
import taghandlers.basic.Save;

/**
 *
 * @author Thilok
 */
public class Basic_Save extends TagComponent{
    private Object object;
    public Basic_Save(Save save){
        this.object=save.getObject();
    }

    @Override
    public void execute() {
        Transaction tx = null;
        Session session=this.getS();
        tx = session.beginTransaction();
        session.save(getObject());
        tx.commit();
        session.close();
    }

    /**
     * @return the object
     */
    public Object getObject() {
        return object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }

    

}
