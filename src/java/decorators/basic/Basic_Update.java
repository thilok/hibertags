/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.basic;

import decorators.TagComponent;
import org.hibernate.Session;
import org.hibernate.Transaction;
import taghandlers.basic.Update;

/**
 *
 * @author Thilok
 */
public class Basic_Update extends TagComponent {

    private Object object;

    public Basic_Update(Update update) {
        this.object = update.getObject();
    }

    @Override
    public void execute() {
        Transaction tx = null;
        Session session = this.getS();
        tx = session.beginTransaction();
        session.update(getObject());
        tx.commit();
        session.close();
    }

    /**
     * @return the object
     */
    public Object getObject() {
        return object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }
}
