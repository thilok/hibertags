/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.aggregations;

import decorators.TagComponent;
import org.hibernate.criterion.Projections;

/**
 *
 * @author Thilok
 */
public class Aggregations_DisplayId extends TagComponent {

    private String className;

    public Aggregations_DisplayId(taghandlers.aggregations.DisplayId displayId) {
        this.className=displayId.getClassName();
    }

    @Override
    public void execute() {
        this.setList(this.getS().createCriteria(this.getClassName()).setProjection(Projections.id()).list());
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }
}
