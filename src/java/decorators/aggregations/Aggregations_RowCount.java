/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package decorators.aggregations;

import decorators.TagComponent;
import org.hibernate.criterion.Projections;

/**
 *
 * @author Thilok
 */
public class Aggregations_RowCount extends TagComponent{

    private String className;

    public Aggregations_RowCount(taghandlers.aggregations.RowCount rowCount){
        this.className=rowCount.getClassName();
    }

    @Override
    public void execute() {
        this.setList(this.getS().createCriteria(this.getClassName()).setProjection(Projections.rowCount()).list());
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }
}
