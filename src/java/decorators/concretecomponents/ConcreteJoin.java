/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package decorators.concretecomponents;

import decorators.TagComponent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.criteria.From;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;

/**
 *
 * @author Thilok
 */
public class ConcreteJoin extends TagComponent{
    private String className;
    private String associationPath;
    private String alias;
    private String orderType=null;
    private String orderBy=null;
    private boolean uniqueResult=false;
    

    public ConcreteJoin(String className,String associationPath, String alias, String orderType, String orderBy, boolean uniqueResult){
        this.className=className;
        this.associationPath=associationPath;
        this.alias=alias;
        this.orderType=orderType;
        
        this.uniqueResult=uniqueResult;
        this.orderBy=orderBy;

    }

    @Override
    public void execute() {
        try {


            Class cls = Class.forName(getClassName());
            this.setDc(DetachedCriteria.forClass(cls));
            this.setDc(this.getDc().createAlias(this.getAssociationPath(), this.getAlias()));

            

            if (getOrderType() != null && getOrderBy() != null) {
                if (getOrderType().equals("asc")) {
                    this.setDc(this.getDc().addOrder(Order.asc(getOrderBy())));
                } else if (getOrderType().equals("desc")) {
                    this.setDc(this.getDc().addOrder(Order.desc(getOrderBy())));
                }
            }
            
            if (isUniqueResult() != false) {
                this.setDc((DetachedCriteria) this.getDc().getExecutableCriteria(getS()).uniqueResult());
            }

            this.setList(this.getDc().getExecutableCriteria(getS()).list());


        } catch (ClassNotFoundException ex) {

            Logger.getLogger(From.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the associationPath
     */
    public String getAssociationPath() {
        return associationPath;
    }

    /**
     * @param associationPath the associationPath to set
     */
    public void setAssociationPath(String associationPath) {
        this.associationPath = associationPath;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the uniqueResult
     */
    public boolean isUniqueResult() {
        return uniqueResult;
    }

    /**
     * @param uniqueResult the uniqueResult to set
     */
    public void setUniqueResult(boolean uniqueResult) {
        this.uniqueResult = uniqueResult;
    }

    

    
}
