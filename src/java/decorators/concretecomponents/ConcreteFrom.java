/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.concretecomponents;

import decorators.TagComponent;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import taghandlers.basic.From;

/**
 *
 * @author Thilok
 */
public class ConcreteFrom extends TagComponent {

    private String className;
    private String orderType=null;
    private String orderBy=null;
    private boolean uniqueResult=false;
    
    
    


    public ConcreteFrom(String className, String orderType, String orderBy, boolean uniqueResult){
        this.className=className;
        this.orderType=orderType;
        
        this.uniqueResult=uniqueResult;
        this.orderBy=orderBy;

    }



    @Override
    public void execute() {
        try {


            Class cls = Class.forName(getClassName());
            this.setDc(DetachedCriteria.forClass(cls));


            if (getOrderType() != null && getOrderBy() != null) {
                if (getOrderType().equals("asc")) {
                    this.setDc(this.getDc().addOrder(Order.asc(getOrderBy())));
                } else if (getOrderType().equals("desc")) {
                    this.setDc(this.getDc().addOrder(Order.desc(getOrderBy())));
                }
            }
            
            if (isUniqueResult() != false) {
                this.setDc((DetachedCriteria) this.getDc().getExecutableCriteria(getS()).uniqueResult());
            }

            this.setList(this.getDc().getExecutableCriteria(getS()).list());


        } catch (ClassNotFoundException ex) {

            Logger.getLogger(From.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the uniqueResult
     */
    public boolean isUniqueResult() {
        return uniqueResult;
    }

    /**
     * @param uniqueResult the uniqueResult to set
     */
    public void setUniqueResult(boolean uniqueResult) {
        this.uniqueResult = uniqueResult;
    }

    

    

    
}
