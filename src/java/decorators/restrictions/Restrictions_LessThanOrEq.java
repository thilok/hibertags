/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.LessThanOrEq;

/**
 *
 * @author Thilok
 */
public class Restrictions_LessThanOrEq extends AbstractRestrictions {

    private String propertyName = null;
    private String value = null;
    private String type = null;

    public Restrictions_LessThanOrEq(TagComponent wrappee, LessThanOrEq lessThanOrEq) {
        this.wrappee = wrappee;
        this.propertyName = lessThanOrEq.getPropertyName();
        this.value = lessThanOrEq.getValue();
        this.type = lessThanOrEq.getType();
    }

    @Override
    public void execute() {
        wrappee.execute();
        this.setDc(wrappee.getDc());

        if (getType().equals("int")) {
            this.setDc(this.getDc().add(Restrictions.le(getPropertyName(), Integer.parseInt(getValue()))));
        } else if (getType().equals("double")) {
            this.setDc(this.getDc().add(Restrictions.le(getPropertyName(), Double.parseDouble(getValue()))));
        } else if (getType().equals("float")) {
            this.setDc(this.getDc().add(Restrictions.le(getPropertyName(), Float.parseFloat(getValue()))));
        }



        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
