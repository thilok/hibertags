/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.NotEq;

/**
 *
 * @author Thilok
 */
public class Restrictions_NotEq extends AbstractRestrictions {

    private String propertyName = null;
    private String value = null;
    private String type=null;
    private boolean ignoreCase = false;

    public Restrictions_NotEq(TagComponent wrappee, NotEq notEq) {
        this.wrappee = wrappee;
        this.ignoreCase = notEq.isIgnoreCase();
        this.value = notEq.getValue();
        this.type=notEq.getType();

        this.propertyName = notEq.getPropertyName();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());

        if (isIgnoreCase() == true) {
            if (getType().equals("int")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), Integer.parseInt(getValue())).ignoreCase()));
            } else if (getType().equals("double")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), Double.parseDouble(getValue())).ignoreCase()));
            }else if (getType().equals("float")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), Float.parseFloat(getValue())).ignoreCase()));
            }else if (getType().equals("string")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), getValue().toString()).ignoreCase()));
            }
            
        } else {
            if (getType().equals("int")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), Integer.parseInt(getValue()))));
            } else if (getType().equals("double")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), Double.parseDouble(getValue()))));
            }else if (getType().equals("float")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), Float.parseFloat(getValue()))));
            }else if (getType().equals("string")) {
                this.setDc(this.getDc().add(Restrictions.ne(getPropertyName(), getValue().toString())));
            }
            
        }

        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    

    /**
     * @return the ignoreCase
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase the ignoreCase to set
     */
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
