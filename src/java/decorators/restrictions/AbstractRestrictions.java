/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;

/**
 *
 * @author Thilok
 */
public abstract class AbstractRestrictions extends TagComponent {

    protected TagComponent wrappee;

    public abstract void execute();
}
