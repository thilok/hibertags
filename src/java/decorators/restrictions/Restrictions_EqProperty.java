/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.EqProperty;

/**
 *
 * @author Thilok
 */
public class Restrictions_EqProperty extends AbstractRestrictions{

    private String propertyName1 = null;
    private String propertyName2 = null;
    private boolean ignoreCase = false;

    public Restrictions_EqProperty(TagComponent wrappee, EqProperty eqProperty) {
        this.wrappee = wrappee;
        this.ignoreCase = eqProperty.isIgnoreCase();
        this.propertyName1 = eqProperty.getPropertyName1();

        this.propertyName2 = eqProperty.getPropertyName2();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());

        if (isIgnoreCase() == true) {
            this.setDc(this.getDc().add(Restrictions.eqProperty(getPropertyName1(), getPropertyName2())));
        } else {
            this.setDc(this.getDc().add(Restrictions.eqProperty(getPropertyName1(), getPropertyName2())));
        }

        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName1
     */
    public String getPropertyName1() {
        return propertyName1;
    }

    /**
     * @param propertyName1 the propertyName1 to set
     */
    public void setPropertyName1(String propertyName1) {
        this.propertyName1 = propertyName1;
    }

    /**
     * @return the propertyName2
     */
    public String getPropertyName2() {
        return propertyName2;
    }

    /**
     * @param propertyName2 the propertyName2 to set
     */
    public void setPropertyName2(String propertyName2) {
        this.propertyName2 = propertyName2;
    }

    /**
     * @return the ignoreCase
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase the ignoreCase to set
     */
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    
}
