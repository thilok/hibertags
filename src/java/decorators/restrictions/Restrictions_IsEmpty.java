/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.IsEmpty;

/**
 *
 * @author Thilok
 */
public class Restrictions_IsEmpty extends AbstractRestrictions {

    private String propertyName = null;

    public Restrictions_IsEmpty(TagComponent wrappee, IsEmpty isEmpty) {
        this.wrappee = wrappee;
        this.propertyName = isEmpty.getPropertyName();
    }

    @Override
    public void execute() {
        wrappee.execute();
        this.setDc(wrappee.getDc());

        this.setDc(this.getDc().add(Restrictions.isEmpty(getPropertyName())));




        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
}
