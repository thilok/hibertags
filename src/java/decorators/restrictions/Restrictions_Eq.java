/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.Eq;

/**
 *
 * @author Thilok
 */
public class Restrictions_Eq extends AbstractRestrictions {

    private String propertyNameForIs = null;
    private String valueForIs = null;
    private String type = null;
    private boolean ignoreCaseForIs = false;

    public Restrictions_Eq(TagComponent wrappee, Eq eq) {
        this.wrappee = wrappee;
        this.ignoreCaseForIs = eq.isIgnoreCase();
        this.type = eq.getType();
        this.valueForIs = eq.getValue();

        this.propertyNameForIs = eq.getPropertyName();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());
        

        if (isIgnoreCaseForIs() == true) {
            if (getType().equals("int")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), Integer.parseInt(getValueForIs())).ignoreCase()));
            } else if (getType().equals("double")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), Double.parseDouble(getValueForIs())).ignoreCase()));
            }else if (getType().equals("float")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), Float.parseFloat(getValueForIs())).ignoreCase()));
            }else if (getType().equals("string")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), getValueForIs().toString()).ignoreCase()));
            }
            
        } else {
            if (getType().equals("int")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), Integer.parseInt(getValueForIs()))));
            } else if (getType().equals("double")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), Double.parseDouble(getValueForIs()))));
            }else if (getType().equals("float")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), Float.parseFloat(getValueForIs()))));
            }else if (getType().equals("string")) {
                this.setDc(this.getDc().add(Restrictions.eq(getPropertyNameForIs(), getValueForIs().toString())));
            }
            
        }

        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyNameForIs
     */
    public String getPropertyNameForIs() {
        return propertyNameForIs;
    }

    /**
     * @param propertyNameForIs the propertyNameForIs to set
     */
    public void setPropertyNameForIs(String propertyNameForIs) {
        this.propertyNameForIs = propertyNameForIs;
    }

    /**
     * @return the valueForIs
     */
    public String getValueForIs() {
        return valueForIs;
    }

    /**
     * @param valueForIs the valueForIs to set
     */
    public void setValueForIs(String valueForIs) {
        this.valueForIs = valueForIs;
    }

    /**
     * @return the ignoreCaseForIs
     */
    public boolean isIgnoreCaseForIs() {
        return ignoreCaseForIs;
    }

    /**
     * @param ignoreCaseForIs the ignoreCaseForIs to set
     */
    public void setIgnoreCaseForIs(boolean ignoreCaseForIs) {
        this.ignoreCaseForIs = ignoreCaseForIs;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
