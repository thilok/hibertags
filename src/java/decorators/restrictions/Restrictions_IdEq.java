/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.IdEq;

/**
 *
 * @author Thilok
 */
public class Restrictions_IdEq extends AbstractRestrictions {

    private String value;
    private String type;

    public Restrictions_IdEq(TagComponent wrappee, IdEq idEq) {
        this.wrappee = wrappee;
        this.value = idEq.getValue();
        this.type = idEq.getType();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());
        

        if (this.getType().equals("int")) {
            this.setDc(this.getDc().add(Restrictions.idEq(Integer.parseInt(getValue()))));
        }
        if (this.getType().equals("double")) {
            this.setDc(this.getDc().add(Restrictions.idEq(Double.parseDouble(getValue()))));
        }
        //find data types allowed by hibernate and continue if conditions

        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
