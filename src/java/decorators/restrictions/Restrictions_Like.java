/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.Like;

/**
 *
 * @author Thilok
 */
public class Restrictions_Like extends AbstractRestrictions {

    private String propertyNameForLike = null;
    private String likeValue = null;
    private String matchMode = null;
    private boolean ignoreCase = false;

    public Restrictions_Like(TagComponent wrappee, Like like) {
        this.wrappee = wrappee;
        this.ignoreCase = like.isIgnoreCase();
        this.likeValue = like.getLikeValue();
        this.matchMode = like.getMatchMode();
        this.propertyNameForLike = like.getPropertyName();
    }

    @Override
    public void execute() {

        wrappee.execute();

        this.setDc(wrappee.getDc());

        if (getPropertyNameForLike() != null && getLikeValue() != null) {

            if (isIgnoreCase() == true && getMatchMode() != null) {

                if (getMatchMode().equals("start")) {

                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.START).ignoreCase()));

                } else if (getMatchMode().equals("end")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.END).ignoreCase()));
                } else if (getMatchMode().equals("anywhere")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.ANYWHERE).ignoreCase()));
                } else if (getMatchMode().equals("exact")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.EXACT).ignoreCase()));
                }



            } else if (getMatchMode() != null && isIgnoreCase() == false) {

                if (getMatchMode().equals("start")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.START)));
                } else if (getMatchMode().equals("end")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.END)));
                } else if (getMatchMode().equals("anywhere")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.ANYWHERE)));
                } else if (getMatchMode().equals("exact")) {
                    this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.EXACT)));
                }
            } else if (isIgnoreCase() == true) {
                this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue()).ignoreCase()));
            } else {
                this.setDc(this.getDc().add(Restrictions.like(getPropertyNameForLike(), getLikeValue())));
            }
        }


        
        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyNameForLike
     */
    public String getPropertyNameForLike() {
        return propertyNameForLike;
    }

    /**
     * @param propertyNameForLike the propertyNameForLike to set
     */
    public void setPropertyNameForLike(String propertyNameForLike) {
        this.propertyNameForLike = propertyNameForLike;
    }

    /**
     * @return the likeValue
     */
    public String getLikeValue() {
        return likeValue;
    }

    /**
     * @param likeValue the likeValue to set
     */
    public void setLikeValue(String likeValue) {
        this.likeValue = likeValue;
    }

    /**
     * @return the matchMode
     */
    public String getMatchMode() {
        return matchMode;
    }

    /**
     * @param matchMode the matchMode to set
     */
    public void setMatchMode(String matchMode) {
        this.matchMode = matchMode;
    }

    /**
     * @return the ignoreCase
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase the ignoreCase to set
     */
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }
}
