/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.restrictions;

import decorators.TagComponent;
import java.math.BigDecimal;
import org.hibernate.criterion.Restrictions;
import taghandlers.restrictions.Between;

/**
 *
 * @author Thilok
 */
public class Restrictions_Between extends AbstractRestrictions {

    private String propertyName = null;
    private String highValue = null;
    private String lowValue = null;
    private String type = null;

    public Restrictions_Between(TagComponent wrappee, Between between) {
        this.wrappee = wrappee;
        this.propertyName = between.getPropertyName();
        this.highValue = between.getHighValue();
        this.type=between.getType();
        this.lowValue = between.getLowValue();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());
        

        if(getType().equals("int")){
            this.setDc(this.getDc().add(Restrictions.between(getPropertyName(), Integer.parseInt(getLowValue()), Integer.parseInt(getHighValue()))));
        }
        else if(getType().equals("double")){
            this.setDc(this.getDc().add(Restrictions.between(getPropertyName(), Double.parseDouble(getLowValue()), Double.parseDouble(getHighValue()))));
        }
        else if(getType().equals("float")){
            this.setDc(this.getDc().add(Restrictions.between(getPropertyName(), Float.parseFloat(getLowValue()), Float.parseFloat(getHighValue()))));
        }



        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the highValue
     */
    public String getHighValue() {
        return highValue;
    }

    /**
     * @param highValue the highValue to set
     */
    public void setHighValue(String highValue) {
        this.highValue = highValue;
    }

    /**
     * @return the lowValue
     */
    public String getLowValue() {
        return lowValue;
    }

    /**
     * @param lowValue the lowValue to set
     */
    public void setLowValue(String lowValue) {
        this.lowValue = lowValue;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
