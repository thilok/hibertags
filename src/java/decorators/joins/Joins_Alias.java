/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.joins;

import decorators.TagComponent;
import taghandlers.joins.Alias;

/**
 *
 * @author Thilok
 */
public class Joins_Alias extends AbstractJoins {

    private String associationPath;
    private String alias;

    public Joins_Alias(TagComponent wrappee, Alias aliasClass) {
        this.wrappee = wrappee;
        this.alias=aliasClass.getAlias();
        this.associationPath=aliasClass.getAssociationPath();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());
        

        this.setDc(this.getDc().createAlias(this.getAssociationPath(), this.getAlias()));

        



        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the associationPath
     */
    public String getAssociationPath() {
        return associationPath;
    }

    /**
     * @param associationPath the associationPath to set
     */
    public void setAssociationPath(String associationPath) {
        this.associationPath = associationPath;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }
}
