/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.joins;

import decorators.TagComponent;






/**
 *
 * @author Thilok
 */
public class Joins_FetchMode extends AbstractJoins {

    private String associationPath;
    private String fetchMode;

    public Joins_FetchMode(TagComponent wrappee, taghandlers.joins.FetchMode fetchModeClass) {
        this.wrappee = wrappee;
        this.fetchMode = fetchModeClass.getFetchMode();
        this.associationPath = fetchModeClass.getAssociationPath();
    }

    @Override
    public void execute() {

        wrappee.execute();
        this.setDc(wrappee.getDc());
        


        if (this.getFetchMode().equals("join")) {
            this.setDc(this.getDc().setFetchMode(this.getAssociationPath(), org.hibernate.FetchMode.JOIN));
        } else if (this.getFetchMode().equals("select")) {
            this.setDc(this.getDc().setFetchMode(this.getAssociationPath(), org.hibernate.FetchMode.SELECT));
        } else if (this.getFetchMode().equals("eager")) {
            this.setDc(this.getDc().setFetchMode(this.getAssociationPath(), org.hibernate.FetchMode.EAGER));
        } else if (this.getFetchMode().equals("lazy")) {
            this.setDc(this.getDc().setFetchMode(this.getAssociationPath(), org.hibernate.FetchMode.LAZY));
        } else if (this.getFetchMode().equals("default") || this.getFetchMode() == null) {
            this.setDc(this.getDc().setFetchMode(this.getAssociationPath(), org.hibernate.FetchMode.DEFAULT));
        }




        this.setList(this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the associationPath
     */
    public String getAssociationPath() {
        return associationPath;
    }

    /**
     * @param associationPath the associationPath to set
     */
    public void setAssociationPath(String associationPath) {
        this.associationPath = associationPath;
    }

    /**
     * @return the fetchMode
     */
    public String getFetchMode() {
        return fetchMode;
    }

    /**
     * @param fetchMode the fetchMode to set
     */
    public void setFetchMode(String fetchMode) {
        this.fetchMode = fetchMode;
    }
}
