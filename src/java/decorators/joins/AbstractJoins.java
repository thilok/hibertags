/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.joins;

import decorators.TagComponent;

/**
 *
 * @author Thilok
 */
public abstract class AbstractJoins extends TagComponent {

    protected TagComponent wrappee;

    public abstract void execute();
}
