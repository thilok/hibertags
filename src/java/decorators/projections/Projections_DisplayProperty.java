/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.projections;

import decorators.TagComponent;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.PageContext;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import taghandlers.projections.DisplayProperty;

/**
 *
 * @author Thilok
 */
public class Projections_DisplayProperty extends AbstractProjections {

    private String propertyName;
    private String var;
    private JspContext jspContext;

    public Projections_DisplayProperty(TagComponent wrappee, DisplayProperty displayProperty, JspContext jspContext) {
        this.jspContext = jspContext;
        this.wrappee = wrappee;
        this.propertyName = displayProperty.getPropertyName();
        this.var = displayProperty.getVar();
    }

    @Override
    public void execute() {
        wrappee.execute();
        this.setDc(wrappee.getDc());
        


        this.setDc(this.getDc().setProjection(Projections.projectionList().add(Projections.property(this.getPropertyName()))));

        this.getJspContext().setAttribute(this.getVar(), this.getDc().getExecutableCriteria(getS()).list());
        



    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the var
     */
    public String getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(String var) {
        this.var = var;
    }

    

    /**
     * @return the jspContext
     */
    public JspContext getJspContext() {
        return jspContext;
    }

    /**
     * @param jspContext the jspContext to set
     */
    public void setJspContext(JspContext jspContext) {
        this.jspContext = jspContext;
    }
}
