/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package decorators.projections;

import decorators.TagComponent;
import javax.servlet.jsp.JspContext;
import org.hibernate.criterion.Projections;
import taghandlers.projections.CountDistinct;

/**
 *
 * @author Thilok
 */
public class Projections_CountDistinct extends AbstractProjections {

    private String propertyName = null;
    private String var = null;
    private JspContext jspContext;

    public Projections_CountDistinct(TagComponent wrappee, CountDistinct countDistinct, JspContext jspContext) {
        this.wrappee = wrappee;
        this.propertyName = countDistinct.getPropertyName();
        this.var = countDistinct.getVar();
        this.jspContext = jspContext;

    }

    @Override
    public void execute() {
        wrappee.execute();
        this.setDc(wrappee.getDc());
        

        this.setDc(this.getDc().setProjection(Projections.projectionList().add(Projections.countDistinct(this.getPropertyName()))));

        this.getJspContext().setAttribute(this.getVar(), this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the var
     */
    public String getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * @return the jspContext
     */
    public JspContext getJspContext() {
        return jspContext;
    }

    /**
     * @param jspContext the jspContext to set
     */
    public void setJspContext(JspContext jspContext) {
        this.jspContext = jspContext;
    }
}
