/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package decorators.projections;

import decorators.TagComponent;
import javax.servlet.jsp.JspContext;
import org.hibernate.criterion.Projections;
import taghandlers.projections.Count;

/**
 *
 * @author Thilok
 */
public class Projections_Count extends AbstractProjections{
private String propertyName = null;
    private String var = null;
    private String attributes=null;
    private JspContext jspContext;

    public Projections_Count(TagComponent wrappee, Count count, JspContext jspContext) {
        this.wrappee = wrappee;
        this.propertyName=count.getPropertyName();
        this.var=count.getVar();
        this.jspContext=jspContext;

    }

    @Override
    public void execute() {
        wrappee.execute();
        this.setDc(wrappee.getDc());
        

        this.setDc(this.getDc().setProjection(Projections.projectionList().add(Projections.count(this.getPropertyName()))));

        this.getJspContext().setAttribute(this.getVar(),this.getDc().getExecutableCriteria(getS()).list());
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the var
     */
    public String getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * @return the attributes
     */
    public String getAttributes() {
        return attributes;
    }

    /**
     * @param attributes the attributes to set
     */
    public void setAttributes(String attributes) {
        this.attributes = attributes;
    }

    /**
     * @return the jspContext
     */
    public JspContext getJspContext() {
        return jspContext;
    }

    /**
     * @param jspContext the jspContext to set
     */
    public void setJspContext(JspContext jspContext) {
        this.jspContext = jspContext;
    }
}
