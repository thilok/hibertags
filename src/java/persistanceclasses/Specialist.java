/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class Specialist {
    private int spId;
    private int noOfProblems;
    private Problem problem;
    private Employee2 employee2;

    public Specialist() {
    }

    public Specialist(int noOfProblems,Problem problem,Employee2 employee2) {

        
        this.noOfProblems=noOfProblems;
        this.problem=problem;
        this.employee2 = employee2;

    }

    /**
     * @return the spId
     */
    public int getSpId() {
        return spId;
    }

    /**
     * @param spId the spId to set
     */
    public void setSpId(int spId) {
        this.spId = spId;
    }

    /**
     * @return the noOfProblems
     */
    public int getNoOfProblems() {
        return noOfProblems;
    }

    /**
     * @param noOfProblems the noOfProblems to set
     */
    public void setNoOfProblems(int noOfProblems) {
        this.noOfProblems = noOfProblems;
    }

    /**
     * @return the problem
     */
    public Problem getProblem() {
        return problem;
    }

    /**
     * @param problem the problem to set
     */
    public void setProblem(Problem problem) {
        this.problem = problem;
    }

    /**
     * @return the employee2
     */
    public Employee2 getEmployee2() {
        return employee2;
    }

    /**
     * @param employee2 the employee2 to set
     */
    public void setEmployee2(Employee2 employee2) {
        this.employee2 = employee2;
    }

}
