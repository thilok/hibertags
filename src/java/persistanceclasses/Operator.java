/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class Operator {

    private int operatorId;
    private Employee2 employee2;

    public Operator() {
    }

    public Operator(Employee2 employee2) {

        this.employee2 = employee2;

    }

    /**
     * @return the operatorId
     */
    public int getOperatorId() {
        return operatorId;
    }

    /**
     * @param operatorId the operatorId to set
     */
    public void setOperatorId(int operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * @return the employee2
     */
    public Employee2 getEmployee2() {
        return employee2;
    }

    /**
     * @param employee2 the employee2 to set
     */
    public void setEmployee2(Employee2 employee2) {
        this.employee2 = employee2;
    }
}
