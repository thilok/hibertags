/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class Logger {
    private int logId;
    private String problemDescription;
    private Caller caller;
    private Operator operator;
    private Specialist specialist;
    private Problem problem;

    public Logger() {
    }

    public Logger(String problemDescription,Caller caller,Problem problem,Specialist specialist,Operator operator) {

        this.problemDescription=problemDescription;
        this.operator=operator;
        this.caller=caller;
        this.problem=problem;
        this.specialist=specialist;

    }

    /**
     * @return the logId
     */
    public int getLogId() {
        return logId;
    }

    /**
     * @param logId the logId to set
     */
    public void setLogId(int logId) {
        this.logId = logId;
    }

    /**
     * @return the problemDescription
     */
    public String getProblemDescription() {
        return problemDescription;
    }

    /**
     * @param problemDescription the problemDescription to set
     */
    public void setProblemDescription(String problemDescription) {
        this.problemDescription = problemDescription;
    }

    /**
     * @return the caller
     */
    public Caller getCaller() {
        return caller;
    }

    /**
     * @param caller the caller to set
     */
    public void setCaller(Caller caller) {
        this.caller = caller;
    }

    /**
     * @return the operator
     */
    public Operator getOperator() {
        return operator;
    }

    /**
     * @param operator the operator to set
     */
    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    /**
     * @return the specialist
     */
    public Specialist getSpecialist() {
        return specialist;
    }

    /**
     * @param specialist the specialist to set
     */
    public void setSpecialist(Specialist specialist) {
        this.specialist = specialist;
    }

    /**
     * @return the problem
     */
    public Problem getProblem() {
        return problem;
    }

    /**
     * @param problem the problem to set
     */
    public void setProblem(Problem problem) {
        this.problem = problem;
    }
}
