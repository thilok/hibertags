/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class LoginValid {
    private int id;
    private String password;
    private String userLevel;
    private String userName;
    private String employeeId;
    /**
     * @return the id
     */
    public LoginValid(){
    }
    public LoginValid(String userName,String password,String userLevel,String employeeId){
        this.userName=userName;
        this.password=password;
        this.userLevel=userLevel;
        this.employeeId=employeeId;
    }
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userLevel
     */
    public String getUserLevel() {
        return userLevel;
    }

    /**
     * @param userLevel the userLevel to set
     */
    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the employeeId
     */
    public String getEmployeeId() {
        return employeeId;
    }

    /**
     * @param employeeId the employeeId to set
     */
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}

