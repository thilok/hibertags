/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class Problem {

    private int problemId;
    private int problemType;

    public Problem() {
    }

    public Problem(int problemType) {

        this.problemType = problemType;

    }

    /**
     * @return the problemId
     */
    public int getProblemId() {
        return problemId;
    }

    /**
     * @param problemId the problemId to set
     */
    public void setProblemId(int problemId) {
        this.problemId = problemId;
    }

    /**
     * @return the problemType
     */
    public int getProblemType() {
        return problemType;
    }

    /**
     * @param problemType the problemType to set
     */
    public void setProblemType(int problemType) {
        this.problemType = problemType;
    }
}
