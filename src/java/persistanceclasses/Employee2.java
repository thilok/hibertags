/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class Employee2 {
    private int empId;
    private String empName;
    private String empDepartment;
    private String empJobTitle;
    

    /**
     * @return the id
     */
    public Employee2(){
    }

    public Employee2(String empName,String empDepartment,String empJobTitle){
        this.empDepartment=empDepartment;
        this.empJobTitle=empJobTitle;
        this.empName=empName;

    }



    /**
     * @return the empName
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * @return the empDepartment
     */
    public String getEmpDepartment() {
        return empDepartment;
    }

    /**
     * @param empDepartment the empDepartment to set
     */
    public void setEmpDepartment(String empDepartment) {
        this.empDepartment = empDepartment;
    }

    /**
     * @return the empJobTitle
     */
    public String getEmpJobTitle() {
        return empJobTitle;
    }

    /**
     * @param empJobTitle the empJobTitle to set
     */
    public void setEmpJobTitle(String empJobTitle) {
        this.empJobTitle = empJobTitle;
    }

    

    /**
     * @return the empId
     */
    public int getEmpId() {
        return empId;
    }

    /**
     * @param empId the empId to set
     */
    public void setEmpId(int empId) {
        this.empId = empId;
    }

}

