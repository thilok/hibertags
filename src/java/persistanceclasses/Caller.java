/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class Caller {

    private int callerId;
    private Employee2 employee2;

    /**
     * @return the id
     */
    public Caller() {
    }

    public Caller(Employee2 employee2) {

        this.employee2 = employee2;

    }

    

    /**
     * @return the employee2
     */
    public Employee2 getEmployee2() {
        return employee2;
    }

    /**
     * @param employee2 the employee2 to set
     */
    public void setEmployee2(Employee2 employee2) {
        this.employee2 = employee2;
    }

    /**
     * @return the callerId
     */
    public int getCallerId() {
        return callerId;
    }

    /**
     * @param callerId the callerId to set
     */
    public void setCallerId(int callerId) {
        this.callerId = callerId;
    }
}
