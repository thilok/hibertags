/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class EmployeeSalary {

    private int id;
    private double salary;
    private String name;
    private String description;
    private int age;
    private String position;

    /**
     * @return the id
     */
    public EmployeeSalary() {
    }

    public EmployeeSalary(String name,String description,int age,String position,double salary) {
        this.name=name;
        this.salary=salary;
        this.description=description;
        this.age=age;
        this.position=position;

    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the salary
     */
    public double getSalary() {
        return salary;
    }

    /**
     * @param salary the salary to set
     */
    public void setSalary(double salary) {
        this.salary = salary;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the position
     */
    public String getPosition() {
        return position;
    }

    /**
     * @param position the position to set
     */
    public void setPosition(String position) {
        this.position = position;
    }
}
