/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistanceclasses;

/**
 *
 * @author Thilok
 */
public class LoginValid2 {
    private int loginValidId;
    private String password;
    private String userLevel;
    private String userName;
    private Employee2 employee2;
    
    /**
     * @return the id
     */
    public LoginValid2(){
    }
    public LoginValid2(String userName,String password,String userLevel,Employee2 employee2){
        this.userName=userName;
        this.password=password;
        this.userLevel=userLevel;
        this.employee2=employee2;
        
    }
    

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the userLevel
     */
    public String getUserLevel() {
        return userLevel;
    }

    /**
     * @param userLevel the userLevel to set
     */
    public void setUserLevel(String userLevel) {
        this.userLevel = userLevel;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    

    /**
     * @return the loginValidId
     */
    public int getLoginValidId() {
        return loginValidId;
    }

    /**
     * @param loginValidId the loginValidId to set
     */
    public void setLoginValidId(int loginValidId) {
        this.loginValidId = loginValidId;
    }

    /**
     * @return the employee2
     */
    public Employee2 getEmployee2() {
        return employee2;
    }

    /**
     * @param employee2 the employee2 to set
     */
    public void setEmployee2(Employee2 employee2) {
        this.employee2 = employee2;
    }
}

