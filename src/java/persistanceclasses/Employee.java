/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package persistanceclasses;

import java.util.List;

/**
 *
 * @author Thilok
 */
public class Employee {
    private int id;
    private String empName;
    private String empDepartment;
    private String empJobTitle;
    private List login;

    /**
     * @return the id
     */
    public Employee(){
    }

    public Employee(String empName,String empDepartment,String empJobTitle){
        this.empDepartment=empDepartment;
        this.empJobTitle=empJobTitle;
        this.empName=empName;

    }

    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the empName
     */
    public String getEmpName() {
        return empName;
    }

    /**
     * @param empName the empName to set
     */
    public void setEmpName(String empName) {
        this.empName = empName;
    }

    /**
     * @return the empDepartment
     */
    public String getEmpDepartment() {
        return empDepartment;
    }

    /**
     * @param empDepartment the empDepartment to set
     */
    public void setEmpDepartment(String empDepartment) {
        this.empDepartment = empDepartment;
    }

    /**
     * @return the empJobTitle
     */
    public String getEmpJobTitle() {
        return empJobTitle;
    }

    /**
     * @param empJobTitle the empJobTitle to set
     */
    public void setEmpJobTitle(String empJobTitle) {
        this.empJobTitle = empJobTitle;
    }

    /**
     * @return the login
     */
    public List getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(List login) {
        this.login = login;
    }

}

