/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taghandlers.aggregations;

import decorators.TagComponent;
import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Thilok
 */
public class RowCount extends SimpleTagSupport{

    private String className;
    private String var;

    public void doTag() throws JspException, IOException {
        TagComponent acc =new decorators.aggregations.Aggregations_RowCount(this);
        acc.execute();
        List l=acc.getList();
        getJspContext().setAttribute(getVar(), l);
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the var
     */
    public String getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(String var) {
        this.var = var;
    }

}
