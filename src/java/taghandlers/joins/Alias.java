/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.joins;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import taghandlers.restrictions.Like;

/**
 *
 * @author Thilok
 */
public class Alias extends SimpleTagSupport{

    private String associationPath;
    private String alias;
    private int nestedCount = 0;

    public void doTag() throws JspException, IOException {
        JspTag parent = null;
        Class c = null;

        SimpleTag parent2 = (SimpleTag) getParent();
        while (parent2 != null) {
            parent2 = (SimpleTag) parent2.getParent();
            setNestedCount(getNestedCount() + 1);
        }

        for (int i = 0; i < getNestedCount(); i++) {
            parent = getParent();
            c = parent.getClass();
        }



        Object obj = null;



        try {

            Method m1 = c.getMethod("setAliasClass", new Class[]{Alias.class});
//            Method m2 = c.getMethod("setValueForIs", new Class[]{String.class});
//
//            Method m3 = c.getMethod("setIgnoreCaseForIs", new Class[]{boolean.class});
            try {
                obj = m1.invoke(parent, this);
//                obj = m2.invoke(parent, new String(getValue()));
//
//                obj = m3.invoke(parent, new Boolean(isIgnoreCase()));
                //getJspContext().getOut().print(o2);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the associationPath
     */
    public String getAssociationPath() {
        return associationPath;
    }

    /**
     * @param associationPath the associationPath to set
     */
    public void setAssociationPath(String associationPath) {
        this.associationPath = associationPath;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the nestedCount
     */
    public int getNestedCount() {
        return nestedCount;
    }

    /**
     * @param nestedCount the nestedCount to set
     */
    public void setNestedCount(int nestedCount) {
        this.nestedCount = nestedCount;
    }
}
