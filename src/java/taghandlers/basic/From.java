/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.basic;

import taghandlers.restrictions.Like;
import taghandlers.restrictions.LessThanOrEq;
import taghandlers.restrictions.IsNull;
import taghandlers.restrictions.NotEq;
import taghandlers.restrictions.Eq;
import taghandlers.restrictions.GreaterThanOrEq;
import taghandlers.restrictions.Between;
import taghandlers.restrictions.GreaterThan;
import taghandlers.restrictions.LessThan;
import taghandlers.restrictions.EqProperty;
import taghandlers.restrictions.IsEmpty;
import taghandlers.restrictions.IsNotEmpty;
import taghandlers.restrictions.IsNotNull;
import decorators.TagComponent;
import decorators.concretecomponents.ConcreteFrom;
import decorators.projections.Projections_AvgOf;
import decorators.restrictions.Restrictions_Between;
import decorators.projections.Projections_Count;
import decorators.projections.Projections_CountDistinct;
import decorators.projections.Projections_DisplayProperty;
import decorators.restrictions.Restrictions_Eq;
import decorators.restrictions.Restrictions_EqProperty;
import decorators.restrictions.Restrictions_GreaterThan;
import decorators.restrictions.Restrictions_GreaterThanOrEq;
import decorators.projections.Projections_GroupBy;
import decorators.restrictions.Restrictions_IdEq;
import decorators.restrictions.Restrictions_IsEmpty;
import decorators.restrictions.Restrictions_IsNotEmpty;
import decorators.restrictions.Restrictions_IsNotNull;
import decorators.restrictions.Restrictions_IsNull;
import decorators.restrictions.Restrictions_LessThan;
import decorators.restrictions.Restrictions_LessThanOrEq;
import decorators.restrictions.Restrictions_Like;
import decorators.projections.Projections_MaxValue;
import decorators.projections.Projections_MinValue;
import decorators.restrictions.Restrictions_NotEq;
import decorators.projections.Projections_SumOf;
import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import taghandlers.projections.AvgOf;
import taghandlers.projections.Count;
import taghandlers.projections.CountDistinct;
import taghandlers.projections.DisplayProperty;
import taghandlers.projections.GroupBy;
import taghandlers.projections.MaxValue;
import taghandlers.projections.MinValue;
import taghandlers.projections.SumOf;
import taghandlers.restrictions.IdEq;

/**
 *
 * @author Thilok
 */
public class From extends SimpleTagSupport {

    private String className;
    private String orderType = null;
    private String orderBy = null;
    private boolean uniqueResult = false;
    
    private String var="";
    private Eq eq = null;
    private Like like = null;
    private NotEq notEq = null;
    private EqProperty eqProperty = null;
    private Between between = null;
    private GreaterThanOrEq greaterThanOrEq = null;
    private GreaterThan greaterThan = null;
    private LessThanOrEq lessThanOrEq = null;
    private LessThan lessThan = null;
    private IsNull isNull = null;
    private IsNotNull isNotNull = null;
    private IsEmpty isEmpty = null;
    private IsNotEmpty isNotEmpty = null;
    private DisplayProperty displayProperty = null;
    private GroupBy groupBy = null;
    private Count count = null;
    private AvgOf avgOf = null;
    private SumOf sumOf=null;
    private MinValue minValue=null;
    private MaxValue maxValue=null;
    private CountDistinct countDistinct=null;
    private IdEq idEq=null;

    public void doTag() throws JspException, IOException {
        getJspBody().invoke(null);

        TagComponent abstractFrom2=new ConcreteFrom(className, orderType, orderBy, uniqueResult);
        //Restrictions
        if(this.getBetween()!=null){
            abstractFrom2=new Restrictions_Between(abstractFrom2, this.getBetween());
        }
        if(this.getEq()!=null){
            abstractFrom2=new Restrictions_Eq(abstractFrom2, this.getEq());
        }
        if(this.getLike()!=null){
            abstractFrom2=new Restrictions_Like(abstractFrom2, this.getLike());
        }
        if(this.getIdEq()!=null){
            abstractFrom2=new Restrictions_IdEq(abstractFrom2, this.getIdEq());
        }
        if(this.getNotEq()!=null){
            abstractFrom2=new Restrictions_NotEq(abstractFrom2, this.getNotEq());
        }
        if(this.getEqProperty()!=null){
            abstractFrom2=new Restrictions_EqProperty(abstractFrom2, this.getEqProperty());
        }
        if(this.getGreaterThanOrEq()!=null){
            abstractFrom2=new Restrictions_GreaterThanOrEq(abstractFrom2, this.getGreaterThanOrEq());
        }
        if(this.getGreaterThan()!=null){
            abstractFrom2=new Restrictions_GreaterThan(abstractFrom2, this.getGreaterThan());
        }
        if(this.getLessThanOrEq()!=null){
            abstractFrom2=new Restrictions_LessThanOrEq(abstractFrom2, this.getLessThanOrEq());
        }
        if(this.getLessThan()!=null){
            abstractFrom2=new Restrictions_LessThan(abstractFrom2, this.getLessThan());
        }
        if(this.getIsNull()!=null){
            abstractFrom2=new Restrictions_IsNull(abstractFrom2, this.getIsNull());
        }
        if(this.getIsNotNull()!=null){
            abstractFrom2=new Restrictions_IsNotNull(abstractFrom2, this.getIsNotNull());
        }
        if(this.getIsEmpty()!=null){
            abstractFrom2=new Restrictions_IsEmpty(abstractFrom2, this.getIsEmpty());
        }
        if(this.getIsNotEmpty()!=null){
            abstractFrom2=new Restrictions_IsNotEmpty(abstractFrom2, this.getIsNotEmpty());
        }



        //Projections
        if(this.getDisplayProperty()!=null){
            abstractFrom2=new Projections_DisplayProperty(abstractFrom2, this.getDisplayProperty(), this.getJspContext());
        }
        if(this.getGroupBy()!=null){
            //if(abstractFrom1==null){
            //    abstractFrom1=(TagComponent) abstractFrom2;
            //}
            abstractFrom2=new Projections_GroupBy(abstractFrom2, this.getGroupBy(), this.getJspContext());
        }
        
        if(this.getCount()!=null){
            //if(abstractFrom1==null){
              //  abstractFrom1=(TagComponent) abstractFrom2;
            //}
            abstractFrom2=new Projections_Count(abstractFrom2, this.getCount(), this.getJspContext());
        }
        if(this.getCountDistinct()!=null){
            abstractFrom2=new Projections_CountDistinct(abstractFrom2, this.getCountDistinct(), this.getJspContext());
        }
        if(this.getAvgOf()!=null){
            abstractFrom2=new Projections_AvgOf(abstractFrom2, this.getAvgOf(), this.getJspContext());
        }
        if(this.getSumOf()!=null){
            abstractFrom2=new Projections_SumOf(abstractFrom2, this.getSumOf(), this.getJspContext());
        }
        if(this.getMinValue()!=null){
            abstractFrom2=new Projections_MinValue(abstractFrom2, this.getMinValue(), this.getJspContext());
        }
        if(this.getMaxValue()!=null){
            abstractFrom2=new Projections_MaxValue(abstractFrom2, this.getMaxValue(), this.getJspContext());
        }
        




        ///////////////////



//        AbstractRestrictions acc2 = new Restrictions_Between(new ConcreteFrom(className, orderType, orderBy, uniqueResult, firstResult, noOfResult), this.getBetween());
//
//        AbstractRestrictions acc4 = new Restrictions_Eq(acc2, this.getEq());
//
//        AbstractProjections acc5=new Projections_DisplayProperty(acc4, this.getDisplayProperty(), getJspContext());
//
//        AbstractProjections acc6=new Projections_GroupBy(acc5, this.getGroupBy(), getJspContext());
//
//        AbstractProjections acc7=new Projections_Count(acc6, this.getCount(), getJspContext());
//
//        AbstractProjections acc8=new Projections_SumOf(acc7, sumOf, getJspContext());
//        AbstractProjections acc9=new Projections_MaxValue(acc8, maxValue, getJspContext());
//        AbstractProjections acc10=new Projections_MinValue(acc9, minValue, getJspContext());



        //TagComponent acc6=new Projections_DisplayProperty(acc4, this.getDisplayProperty(), getJspContext());
        
        //TagComponent acc6 = new Projections_CountDistinct(new ConcreteFrom(className, orderType, orderBy, uniqueResult, firstResult, noOfResult), this.getCountDistinct(), this.getJspContext());
        //TagComponent acc7 = new Projections_MinValue(new ConcreteFrom(className, orderType, orderBy, uniqueResult, firstResult, noOfResult), this.getMinValue(), this.getJspContext());
//        TagComponent acc7 = new Projections_Count(acc6, this.getCount(), this.getJspContext());
        //TagComponent acc5 = new Restrictions_LessThan(acc4, this.getLessThan());

//        TagComponent acc5 = new Restrictions_IsNotNull(new ConcreteFrom(className, orderType, orderBy, uniqueResult, firstResult, noOfResult), this.getIsNotNull());
//        TagComponent acc = new Restrictions_Like(acc5,this.getLike());

        abstractFrom2.execute();
        //if(abstractFrom1!=null){
          //  abstractFrom1.execute();
        //}
        /////// remember this comment     ---------------------------------------------abstractFrom1.execute();
        //acc4.execute();
        List l = abstractFrom2.getList();

        //List l = acc6.getDc().getExecutableCriteria(HibernateUtil.getSessionFactory().openSession()).list();


//        Object o = null;
//
//        Iterator i = l.iterator();
//        while (i.hasNext()) {
//            o = i.next();
//            getJspContext().setAttribute(getDisplayProperty().getVar(), o.toString());
//            //getJspContext().getOut().print(o.toString());
//        }



//        int cnt=0;
//        getJspContext().getOut().print("check " + this.getDisplayProperty().getVar() + " check");
//        while(acc6.getMap().isEmpty()!=true){
//            cnt=cnt+1;
//            //getJspContext().setAttribute(this.getDisplayProperty().getVar(), acc6.getMap().remove(this.getDisplayProperty().getVar()));
//        }
//        getJspContext().getOut().print("cnt "+cnt);
//
//        try {
//            //getJspContext().getOut().print(acc5.getMap().get(this.getDisplayProperty().getVar()));
//        } catch (Exception e) {
//        }



        //getJspContext().setAttribute(getDisplayProperty().getVar(), l);

//        getJspContext().getOut().print(getDisplayProperty().getVar());
//        getJspContext().getOut().print(o.toString());


//        getJspContext().getOut().print("empty list");

//        if (l.isEmpty()) {
//            getJspContext().getOut().print("empty list");
//        }



//        // 'items' is a deferred-value
//// Get the current EL VariableMapper
//        VariableMapper vm =
//                getJspContext().getELContext().getVariableMapper();
//
//        ValueExpression ve=vm.resolveVariable(this.getVar());
//
//        try{
//        //ve.setValue(getJspContext().getELContext(), o.toString());
//        }catch(Exception e){}
//
//        ValueExpression ve2=vm.setVariable(this.getVar(), ve); // Create an expression to be assigned to the variable
//                // specified in the 'var' attribute.
//                // 'index' is an iteration counter kept by the tag handler.
////        IndexedExpression expr =
////                new IndexExpression(getItems(), index);
//// Assign the expression to the variable specified in
//// the 'var' attribute, so any reference to that variable
//// will be replaced by the expression in subsequent EL
//// evaluations.
//        //ValueExpression oldMapping = vm.setVariable(getVar(), expr);
//
//        getJspContext().getELContext().getVariableMapper().setVariable(getDisplayProperty().getVar(), ve2);





















        getJspContext().setAttribute(getVar(), l);




//            getJspBody().invoke(null);
//            Session s = HibernateUtil.getSessionFactory().openSession();
//            Class cls = Class.forName(getClassName());
//            DetachedCriteria dc = DetachedCriteria.forClass(cls);
//
//            if (getOrderType() != null && getOrderBy() != null) {
//                if (getOrderType() == "asc") {
//                    dc.addOrder(Order.asc(getOrderBy()));
//                } else if (getOrderType() == "desc") {
//                    dc.addOrder(Order.desc(getOrderBy()));
//                }
//            }
//            if (getNoOfResult() != 0) {
//                dc.getExecutableCriteria(s).setMaxResults(getNoOfResult());
//            }
//            if (getFirstResult() != 0) {
//                dc.getExecutableCriteria(s).setFirstResult(getFirstResult());
//            }
//            if (isUniqueResult() != false) {
//                dc.getExecutableCriteria(s).uniqueResult();
//            }
//
//            //hib:like tag
//            getJspContext().getOut().print(getPropertyNameForLike() + getLikeValue() + getMatchMode() + isIgnoreCase());
//            if (getPropertyNameForLike() != null && getLikeValue() != null) {
//
//                if (isIgnoreCase() == true && getMatchMode() != null) {
//                    //getJspContext().getOut().print("inside");
//                    if (getMatchMode().equals("start")) {
//
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.START).ignoreCase());
//
//                    } else if (getMatchMode().equals("end")) {
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.END).ignoreCase());
//                    } else if (getMatchMode().equals("anywhere")) {
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.ANYWHERE).ignoreCase());
//                    } else if (getMatchMode().equals("exact")) {
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.EXACT).ignoreCase());
//                    }
//
//
//
//                } else if (getMatchMode() != null && isIgnoreCase() == false) {
//                    //getJspContext().getOut().print("inside");
//                    //dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), getMatchMode())); //can use a switch for different match modes
//                    if (getMatchMode().equals("start")) {
//                        //getJspContext().getOut().print("inside");
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.START));
//                    } else if (getMatchMode().equals("end")) {
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.END));
//                    } else if (getMatchMode().equals("anywhere")) {
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.ANYWHERE));
//                    } else if (getMatchMode().equals("exact")) {
//                        dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue(), MatchMode.EXACT));
//                    }
//                } else if (isIgnoreCase() == true) {
//                    dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue()).ignoreCase());
//                } else {
//                    dc.add(Restrictions.like(getPropertyNameForLike(), getLikeValue()));
//                }
//            }
//            //
//            //hib:eq tag
//            if (isIgnoreCaseForIs() == true) {
//                dc.add(Restrictions.eq(getPropertyNameForIs(), getValueForIs()).ignoreCase());
//            } else {
//                dc.add(Restrictions.eq(getPropertyNameForIs(), getValueForIs()));
//            }
//            //
//
//
//
//
//
//            List l = dc.getExecutableCriteria(s).list();
//
//            getJspContext().setAttribute(getVar(), l);
//
//
//        } catch (ClassNotFoundException ex) {
//            getJspContext().getOut().print("error");
//            Logger.getLogger(From.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the var
     */
    public String getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * @return the uniqueResult
     */
    public boolean isUniqueResult() {
        return uniqueResult;
    }

    /**
     * @param uniqueResult the uniqueResult to set
     */
    public void setUniqueResult(boolean uniqueResult) {
        this.uniqueResult = uniqueResult;
    }

    

    /**
     * @return the eq
     */
    public Eq getEq() {
        return eq;
    }

    /**
     * @param eq the eq to set
     */
    public void setEq(Eq eq) {
        this.eq = eq;
    }

    /**
     * @return the like
     */
    public Like getLike() {
        return like;
    }

    /**
     * @param like the like to set
     */
    public void setLike(Like like) {
        this.like = like;
    }

    /**
     * @return the notEq
     */
    public NotEq getNotEq() {
        return notEq;
    }

    /**
     * @param notEq the notEq to set
     */
    public void setNotEq(NotEq notEq) {
        this.notEq = notEq;
    }

    /**
     * @return the eqProperty
     */
    public EqProperty getEqProperty() {
        return eqProperty;
    }

    /**
     * @param eqProperty the eqProperty to set
     */
    public void setEqProperty(EqProperty eqProperty) {
        this.eqProperty = eqProperty;
    }

    /**
     * @return the between
     */
    public Between getBetween() {
        return between;
    }

    /**
     * @param between the between to set
     */
    public void setBetween(Between between) {
        this.between = between;
    }

    /**
     * @return the greaterThanOrEq
     */
    public GreaterThanOrEq getGreaterThanOrEq() {
        return greaterThanOrEq;
    }

    /**
     * @param greaterThanOrEq the greaterThanOrEq to set
     */
    public void setGreaterThanOrEq(GreaterThanOrEq greaterThanOrEq) {
        this.greaterThanOrEq = greaterThanOrEq;
    }

    /**
     * @return the greaterThan
     */
    public GreaterThan getGreaterThan() {
        return greaterThan;
    }

    /**
     * @param greaterThan the greaterThan to set
     */
    public void setGreaterThan(GreaterThan greaterThan) {
        this.greaterThan = greaterThan;
    }

    /**
     * @return the lessThanOrEq
     */
    public LessThanOrEq getLessThanOrEq() {
        return lessThanOrEq;
    }

    /**
     * @param lessThanOrEq the lessThanOrEq to set
     */
    public void setLessThanOrEq(LessThanOrEq lessThanOrEq) {
        this.lessThanOrEq = lessThanOrEq;
    }

    /**
     * @return the lessThan
     */
    public LessThan getLessThan() {
        return lessThan;
    }

    /**
     * @param lessThan the lessThan to set
     */
    public void setLessThan(LessThan lessThan) {
        this.lessThan = lessThan;
    }

    /**
     * @return the isNull
     */
    public IsNull getIsNull() {
        return isNull;
    }

    /**
     * @param isNull the isNull to set
     */
    public void setIsNull(IsNull isNull) {
        this.isNull = isNull;
    }

    /**
     * @return the isNotNull
     */
    public IsNotNull getIsNotNull() {
        return isNotNull;
    }

    /**
     * @param isNotNull the isNotNull to set
     */
    public void setIsNotNull(IsNotNull isNotNull) {
        this.isNotNull = isNotNull;
    }

    /**
     * @return the isEmpty
     */
    public IsEmpty getIsEmpty() {
        return isEmpty;
    }

    /**
     * @param isEmpty the isEmpty to set
     */
    public void setIsEmpty(IsEmpty isEmpty) {
        this.isEmpty = isEmpty;
    }

    /**
     * @return the isNotEmpty
     */
    public IsNotEmpty getIsNotEmpty() {
        return isNotEmpty;
    }

    /**
     * @param isNotEmpty the isNotEmpty to set
     */
    public void setIsNotEmpty(IsNotEmpty isNotEmpty) {
        this.isNotEmpty = isNotEmpty;
    }

    /**
     * @return the displayProperty
     */
    public DisplayProperty getDisplayProperty() {
        return displayProperty;
    }

    /**
     * @param displayProperty the displayProperty to set
     */
    public void setDisplayProperty(DisplayProperty displayProperty) {
        this.displayProperty = displayProperty;
    }

    /**
     * @return the groupBy
     */
    public GroupBy getGroupBy() {
        return groupBy;
    }

    /**
     * @param groupBy the groupBy to set
     */
    public void setGroupBy(GroupBy groupBy) {
        this.groupBy = groupBy;
    }

    /**
     * @return the count
     */
    public Count getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Count count) {
        this.count = count;
    }

    /**
     * @return the avgOf
     */
    public AvgOf getAvgOf() {
        return avgOf;
    }

    /**
     * @param avgOf the avgOf to set
     */
    public void setAvgOf(AvgOf avgOf) {
        this.avgOf = avgOf;
    }

    /**
     * @return the sumOf
     */
    public SumOf getSumOf() {
        return sumOf;
    }

    /**
     * @param sumOf the sumOf to set
     */
    public void setSumOf(SumOf sumOf) {
        this.sumOf = sumOf;
    }

    /**
     * @return the minValue
     */
    public MinValue getMinValue() {
        return minValue;
    }

    /**
     * @param minValue the minValue to set
     */
    public void setMinValue(MinValue minValue) {
        this.minValue = minValue;
    }

    /**
     * @return the maxValue
     */
    public MaxValue getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(MaxValue maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * @return the countDistinct
     */
    public CountDistinct getCountDistinct() {
        return countDistinct;
    }

    /**
     * @param countDistinct the countDistinct to set
     */
    public void setCountDistinct(CountDistinct countDistinct) {
        this.countDistinct = countDistinct;
    }

    /**
     * @return the idEq
     */
    public IdEq getIdEq() {
        return idEq;
    }

    /**
     * @param idEq the idEq to set
     */
    public void setIdEq(IdEq idEq) {
        this.idEq = idEq;
    }
}
