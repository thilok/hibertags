/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package taghandlers.basic;

import decorators.TagComponent;
import decorators.concretecomponents.ConcreteJoin;
import decorators.joins.Joins_Alias;
import decorators.projections.Projections_AvgOf;
import decorators.restrictions.Restrictions_Between;
import decorators.projections.Projections_Count;
import decorators.projections.Projections_CountDistinct;
import decorators.projections.Projections_DisplayProperty;
import decorators.restrictions.Restrictions_Eq;
import decorators.restrictions.Restrictions_EqProperty;
import decorators.joins.Joins_FetchMode;
import decorators.restrictions.Restrictions_GreaterThan;
import decorators.restrictions.Restrictions_GreaterThanOrEq;
import decorators.projections.Projections_GroupBy;
import decorators.restrictions.Restrictions_IdEq;
import decorators.restrictions.Restrictions_IsEmpty;
import decorators.restrictions.Restrictions_IsNotEmpty;
import decorators.restrictions.Restrictions_IsNotNull;
import decorators.restrictions.Restrictions_IsNull;
import decorators.restrictions.Restrictions_LessThan;
import decorators.restrictions.Restrictions_LessThanOrEq;
import decorators.restrictions.Restrictions_Like;
import decorators.projections.Projections_MaxValue;
import decorators.projections.Projections_MinValue;
import decorators.restrictions.Restrictions_NotEq;
import decorators.projections.Projections_SumOf;
import java.io.IOException;
import java.util.List;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import taghandlers.projections.AvgOf;
import taghandlers.projections.Count;
import taghandlers.projections.CountDistinct;
import taghandlers.projections.DisplayProperty;
import taghandlers.projections.GroupBy;
import taghandlers.projections.MaxValue;
import taghandlers.projections.MinValue;
import taghandlers.projections.SumOf;
import taghandlers.joins.Alias;
import taghandlers.restrictions.Between;
import taghandlers.restrictions.Eq;
import taghandlers.restrictions.EqProperty;
import taghandlers.joins.FetchMode;
import taghandlers.restrictions.GreaterThan;
import taghandlers.restrictions.GreaterThanOrEq;
import taghandlers.restrictions.IdEq;
import taghandlers.restrictions.IsEmpty;
import taghandlers.restrictions.IsNotEmpty;
import taghandlers.restrictions.IsNotNull;
import taghandlers.restrictions.IsNull;
import taghandlers.restrictions.LessThan;
import taghandlers.restrictions.LessThanOrEq;
import taghandlers.restrictions.Like;
import taghandlers.restrictions.NotEq;

/**
 *
 * @author Thilok
 */
public class Join extends SimpleTagSupport{
    private String className;
    private String associationPath;
    private String alias;
    private String orderType = null;
    private String orderBy = null;
    private boolean uniqueResult = false;
    
    private String var = "";
    private Eq eq = null;
    private Like like = null;
    private NotEq notEq = null;
    private EqProperty eqProperty = null;
    private Between between = null;
    private GreaterThanOrEq greaterThanOrEq = null;
    private GreaterThan greaterThan = null;
    private LessThanOrEq lessThanOrEq = null;
    private LessThan lessThan = null;
    private IsNull isNull = null;
    private IsNotNull isNotNull = null;
    private IsEmpty isEmpty = null;
    private IsNotEmpty isNotEmpty = null;
    private DisplayProperty displayProperty = null;
    private GroupBy groupBy = null;
    private Count count = null;
    private AvgOf avgOf = null;
    private SumOf sumOf=null;
    private MinValue minValue=null;
    private MaxValue maxValue=null;
    private CountDistinct countDistinct=null;
    private IdEq idEq=null;
    private Alias aliasClass=null;
    private FetchMode fetchModeClass=null;

    public void doTag() throws JspException, IOException {


        //try {
        getJspBody().invoke(null);



        TagComponent abstractFrom2=new ConcreteJoin(className, associationPath, alias, orderType, orderBy, uniqueResult);
        if(this.getAliasClass()!=null){
            abstractFrom2=new Joins_Alias(abstractFrom2, this.getAliasClass());
        }
        if(this.getFetchModeClass()!=null){
            abstractFrom2=new Joins_FetchMode(abstractFrom2, this.getFetchModeClass());
        }
        //Restrictions
        if(this.getBetween()!=null){
            abstractFrom2=new Restrictions_Between(abstractFrom2, this.getBetween());
        }
        if(this.getEq()!=null){
            abstractFrom2=new Restrictions_Eq(abstractFrom2, this.getEq());
        }
        if(this.getLike()!=null){
            abstractFrom2=new Restrictions_Like(abstractFrom2, this.getLike());
        }
        if(this.getIdEq()!=null){
            abstractFrom2=new Restrictions_IdEq(abstractFrom2, this.getIdEq());
        }
        if(this.getNotEq()!=null){
            abstractFrom2=new Restrictions_NotEq(abstractFrom2, this.getNotEq());
        }
        if(this.getEqProperty()!=null){
            abstractFrom2=new Restrictions_EqProperty(abstractFrom2, this.getEqProperty());
        }
        if(this.getGreaterThanOrEq()!=null){
            abstractFrom2=new Restrictions_GreaterThanOrEq(abstractFrom2, this.getGreaterThanOrEq());
        }
        if(this.getGreaterThan()!=null){
            abstractFrom2=new Restrictions_GreaterThan(abstractFrom2, this.getGreaterThan());
        }
        if(this.getLessThanOrEq()!=null){
            abstractFrom2=new Restrictions_LessThanOrEq(abstractFrom2, this.getLessThanOrEq());
        }
        if(this.getLessThan()!=null){
            abstractFrom2=new Restrictions_LessThan(abstractFrom2, this.getLessThan());
        }
        if(this.getIsNull()!=null){
            abstractFrom2=new Restrictions_IsNull(abstractFrom2, this.getIsNull());
        }
        if(this.getIsNotNull()!=null){
            abstractFrom2=new Restrictions_IsNotNull(abstractFrom2, this.getIsNotNull());
        }
        if(this.getIsEmpty()!=null){
            abstractFrom2=new Restrictions_IsEmpty(abstractFrom2, this.getIsEmpty());
        }
        if(this.getIsNotEmpty()!=null){
            abstractFrom2=new Restrictions_IsNotEmpty(abstractFrom2, this.getIsNotEmpty());
        }



        //Projections
        if(this.getDisplayProperty()!=null){
            abstractFrom2=new Projections_DisplayProperty(abstractFrom2, this.getDisplayProperty(), this.getJspContext());
        }
        if(this.getGroupBy()!=null){
            //if(abstractFrom1==null){
            //    abstractFrom1=(TagComponent) abstractFrom2;
            //}
            abstractFrom2=new Projections_GroupBy(abstractFrom2, this.getGroupBy(), this.getJspContext());
        }

        if(this.getCount()!=null){
            //if(abstractFrom1==null){
              //  abstractFrom1=(TagComponent) abstractFrom2;
            //}
            abstractFrom2=new Projections_Count(abstractFrom2, this.getCount(), this.getJspContext());
        }
        if(this.getCountDistinct()!=null){
            abstractFrom2=new Projections_CountDistinct(abstractFrom2, this.getCountDistinct(), this.getJspContext());
        }
        if(this.getAvgOf()!=null){
            abstractFrom2=new Projections_AvgOf(abstractFrom2, this.getAvgOf(), this.getJspContext());
        }
        if(this.getSumOf()!=null){
            abstractFrom2=new Projections_SumOf(abstractFrom2, this.getSumOf(), this.getJspContext());
        }
        if(this.getMinValue()!=null){
            abstractFrom2=new Projections_MinValue(abstractFrom2, this.getMinValue(), this.getJspContext());
        }
        if(this.getMaxValue()!=null){
            abstractFrom2=new Projections_MaxValue(abstractFrom2, this.getMaxValue(), this.getJspContext());
        }



//        TagComponent af=new ConcreteJoin(className, associationPath, alias, orderType, orderBy, uniqueResult, firstResult, noOfResult);
//        af=new Joins_Alias(af, this.getAliasClass());
//        af=new Joins_FetchMode(af, this.getFetchModeClass());
//        af=new Restrictions_GreaterThan(af, this.getGreaterThan());

        
        

        abstractFrom2.execute();
        List l = abstractFrom2.getList();

        getJspContext().setAttribute(getVar(), l);


    }

    /**
     * @return the className
     */
    public String getClassName() {
        return className;
    }

    /**
     * @param className the className to set
     */
    public void setClassName(String className) {
        this.className = className;
    }

    /**
     * @return the associationPath
     */
    public String getAssociationPath() {
        return associationPath;
    }

    /**
     * @param associationPath the associationPath to set
     */
    public void setAssociationPath(String associationPath) {
        this.associationPath = associationPath;
    }

    /**
     * @return the alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * @param alias the alias to set
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * @return the orderType
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * @param orderType the orderType to set
     */
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    /**
     * @return the orderBy
     */
    public String getOrderBy() {
        return orderBy;
    }

    /**
     * @param orderBy the orderBy to set
     */
    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    /**
     * @return the uniqueResult
     */
    public boolean isUniqueResult() {
        return uniqueResult;
    }

    /**
     * @param uniqueResult the uniqueResult to set
     */
    public void setUniqueResult(boolean uniqueResult) {
        this.uniqueResult = uniqueResult;
    }

    

    /**
     * @return the var
     */
    public String getVar() {
        return var;
    }

    /**
     * @param var the var to set
     */
    public void setVar(String var) {
        this.var = var;
    }

    /**
     * @return the eq
     */
    public Eq getEq() {
        return eq;
    }

    /**
     * @param eq the eq to set
     */
    public void setEq(Eq eq) {
        this.eq = eq;
    }

    /**
     * @return the like
     */
    public Like getLike() {
        return like;
    }

    /**
     * @param like the like to set
     */
    public void setLike(Like like) {
        this.like = like;
    }

    /**
     * @return the notEq
     */
    public NotEq getNotEq() {
        return notEq;
    }

    /**
     * @param notEq the notEq to set
     */
    public void setNotEq(NotEq notEq) {
        this.notEq = notEq;
    }

    /**
     * @return the eqProperty
     */
    public EqProperty getEqProperty() {
        return eqProperty;
    }

    /**
     * @param eqProperty the eqProperty to set
     */
    public void setEqProperty(EqProperty eqProperty) {
        this.eqProperty = eqProperty;
    }

    /**
     * @return the between
     */
    public Between getBetween() {
        return between;
    }

    /**
     * @param between the between to set
     */
    public void setBetween(Between between) {
        this.between = between;
    }

    /**
     * @return the greaterThanOrEq
     */
    public GreaterThanOrEq getGreaterThanOrEq() {
        return greaterThanOrEq;
    }

    /**
     * @param greaterThanOrEq the greaterThanOrEq to set
     */
    public void setGreaterThanOrEq(GreaterThanOrEq greaterThanOrEq) {
        this.greaterThanOrEq = greaterThanOrEq;
    }

    /**
     * @return the greaterThan
     */
    public GreaterThan getGreaterThan() {
        return greaterThan;
    }

    /**
     * @param greaterThan the greaterThan to set
     */
    public void setGreaterThan(GreaterThan greaterThan) {
        this.greaterThan = greaterThan;
    }

    /**
     * @return the lessThanOrEq
     */
    public LessThanOrEq getLessThanOrEq() {
        return lessThanOrEq;
    }

    /**
     * @param lessThanOrEq the lessThanOrEq to set
     */
    public void setLessThanOrEq(LessThanOrEq lessThanOrEq) {
        this.lessThanOrEq = lessThanOrEq;
    }

    /**
     * @return the lessThan
     */
    public LessThan getLessThan() {
        return lessThan;
    }

    /**
     * @param lessThan the lessThan to set
     */
    public void setLessThan(LessThan lessThan) {
        this.lessThan = lessThan;
    }

    /**
     * @return the isNull
     */
    public IsNull getIsNull() {
        return isNull;
    }

    /**
     * @param isNull the isNull to set
     */
    public void setIsNull(IsNull isNull) {
        this.isNull = isNull;
    }

    /**
     * @return the isNotNull
     */
    public IsNotNull getIsNotNull() {
        return isNotNull;
    }

    /**
     * @param isNotNull the isNotNull to set
     */
    public void setIsNotNull(IsNotNull isNotNull) {
        this.isNotNull = isNotNull;
    }

    /**
     * @return the isEmpty
     */
    public IsEmpty getIsEmpty() {
        return isEmpty;
    }

    /**
     * @param isEmpty the isEmpty to set
     */
    public void setIsEmpty(IsEmpty isEmpty) {
        this.isEmpty = isEmpty;
    }

    /**
     * @return the isNotEmpty
     */
    public IsNotEmpty getIsNotEmpty() {
        return isNotEmpty;
    }

    /**
     * @param isNotEmpty the isNotEmpty to set
     */
    public void setIsNotEmpty(IsNotEmpty isNotEmpty) {
        this.isNotEmpty = isNotEmpty;
    }

    /**
     * @return the displayProperty
     */
    public DisplayProperty getDisplayProperty() {
        return displayProperty;
    }

    /**
     * @param displayProperty the displayProperty to set
     */
    public void setDisplayProperty(DisplayProperty displayProperty) {
        this.displayProperty = displayProperty;
    }

    /**
     * @return the groupBy
     */
    public GroupBy getGroupBy() {
        return groupBy;
    }

    /**
     * @param groupBy the groupBy to set
     */
    public void setGroupBy(GroupBy groupBy) {
        this.groupBy = groupBy;
    }

    /**
     * @return the count
     */
    public Count getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(Count count) {
        this.count = count;
    }

    /**
     * @return the avgOf
     */
    public AvgOf getAvgOf() {
        return avgOf;
    }

    /**
     * @param avgOf the avgOf to set
     */
    public void setAvgOf(AvgOf avgOf) {
        this.avgOf = avgOf;
    }

    /**
     * @return the sumOf
     */
    public SumOf getSumOf() {
        return sumOf;
    }

    /**
     * @param sumOf the sumOf to set
     */
    public void setSumOf(SumOf sumOf) {
        this.sumOf = sumOf;
    }

    /**
     * @return the minValue
     */
    public MinValue getMinValue() {
        return minValue;
    }

    /**
     * @param minValue the minValue to set
     */
    public void setMinValue(MinValue minValue) {
        this.minValue = minValue;
    }

    /**
     * @return the maxValue
     */
    public MaxValue getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(MaxValue maxValue) {
        this.maxValue = maxValue;
    }

    /**
     * @return the countDistinct
     */
    public CountDistinct getCountDistinct() {
        return countDistinct;
    }

    /**
     * @param countDistinct the countDistinct to set
     */
    public void setCountDistinct(CountDistinct countDistinct) {
        this.countDistinct = countDistinct;
    }

    /**
     * @return the idEq
     */
    public IdEq getIdEq() {
        return idEq;
    }

    /**
     * @param idEq the idEq to set
     */
    public void setIdEq(IdEq idEq) {
        this.idEq = idEq;
    }

    /**
     * @return the aliasClass
     */
    public Alias getAliasClass() {
        return aliasClass;
    }

    /**
     * @param aliasClass the aliasClass to set
     */
    public void setAliasClass(Alias aliasClass) {
        this.aliasClass = aliasClass;
    }

    /**
     * @return the fetchModeClass
     */
    public FetchMode getFetchModeClass() {
        return fetchModeClass;
    }

    /**
     * @param fetchModeClass the fetchModeClass to set
     */
    public void setFetchModeClass(FetchMode fetchModeClass) {
        this.fetchModeClass = fetchModeClass;
    }
}
