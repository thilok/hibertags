/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.basic;

import decorators.TagComponent;
import decorators.basic.Basic_Delete;
import decorators.basic.Basic_Update;
import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Thilok
 */
public class Delete extends SimpleTagSupport {

    private Object object;

    public void doTag() throws JspException, IOException {
        TagComponent acc = new Basic_Delete(this);
        acc.execute();

    }

    /**
     * @return the object
     */
    public Object getObject() {
        return object;
    }

    /**
     * @param object the object to set
     */
    public void setObject(Object object) {
        this.object = object;
    }
}
