/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.restrictions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Thilok
 */
public class IsNotEmpty extends SimpleTagSupport {

    private String propertyName;
    private int nestedCount = 0;

    public void doTag() throws JspException, IOException {
        JspTag parent = null;
        Class c = null;

        SimpleTag parent2 = (SimpleTag) getParent();
        while (parent2 != null) {
            parent2 = (SimpleTag) parent2.getParent();
            setNestedCount(getNestedCount() + 1);
        }

        for (int i = 0; i < getNestedCount(); i++) {
            parent = getParent();
            c = parent.getClass();
        }



        Object obj = null;



        try {

            Method m1 = c.getMethod("setIsNotEmpty", new Class[]{IsNotEmpty.class});
//            Method m2 = c.getMethod("setValueForIs", new Class[]{String.class});
//
//            Method m3 = c.getMethod("setIgnoreCaseForIs", new Class[]{boolean.class});
            try {
                obj = m1.invoke(parent, this);
//                obj = m2.invoke(parent, new String(getValue()));
//
//                obj = m3.invoke(parent, new Boolean(isIgnoreCase()));
                //getJspContext().getOut().print(o2);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the nestedCount
     */
    public int getNestedCount() {
        return nestedCount;
    }

    /**
     * @param nestedCount the nestedCount to set
     */
    public void setNestedCount(int nestedCount) {
        this.nestedCount = nestedCount;
    }
}
