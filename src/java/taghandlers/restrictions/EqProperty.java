/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.restrictions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Thilok
 */
public class EqProperty extends SimpleTagSupport {

    private String propertyName1;
    private String propertyName2;
    private boolean ignoreCase;
    private int nestedCount = 0;

    public void doTag() throws JspException, IOException {
        JspTag parent = null;
        Class c = null;

        SimpleTag parent2 = (SimpleTag) getParent();
        while (parent2 != null) {
            parent2 = (SimpleTag) parent2.getParent();
            setNestedCount(getNestedCount() + 1);
        }

        for (int i = 0; i < getNestedCount(); i++) {
            parent = getParent();
            c = parent.getClass();
        }



        Object obj = null;



        try {

            Method m1 = c.getMethod("setEqProperty", new Class[]{EqProperty.class});
//            Method m2 = c.getMethod("setValueForIs", new Class[]{String.class});
//
//            Method m3 = c.getMethod("setIgnoreCaseForIs", new Class[]{boolean.class});
            try {
                obj = m1.invoke(parent, this);
//                obj = m2.invoke(parent, new String(getValue()));
//
//                obj = m3.invoke(parent, new Boolean(isIgnoreCase()));
                //getJspContext().getOut().print(o2);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the propertyName1
     */
    public String getPropertyName1() {
        return propertyName1;
    }

    /**
     * @param propertyName1 the propertyName1 to set
     */
    public void setPropertyName1(String propertyName1) {
        this.propertyName1 = propertyName1;
    }

    /**
     * @return the propertyName2
     */
    public String getPropertyName2() {
        return propertyName2;
    }

    /**
     * @param propertyName2 the propertyName2 to set
     */
    public void setPropertyName2(String propertyName2) {
        this.propertyName2 = propertyName2;
    }

    /**
     * @return the ignoreCase
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase the ignoreCase to set
     */
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    /**
     * @return the nestedCount
     */
    public int getNestedCount() {
        return nestedCount;
    }

    /**
     * @param nestedCount the nestedCount to set
     */
    public void setNestedCount(int nestedCount) {
        this.nestedCount = nestedCount;
    }
}
