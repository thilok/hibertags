/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.restrictions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Thilok
 */
public class Like extends SimpleTagSupport {

    private String propertyName;
    private String likeValue;
    private String matchMode;
    private boolean ignoreCase;
    //private Like like=null;
    int nestedCount=0;

    

    public void doTag() throws JspException, IOException {

          //getJspContext().getOut().print(getPropertyName()+getLikeValue());


//        Class c = s.getClass();
//        Method m = c.getMethod("substring", new Class[]{int.class, int.class});
//        Object obj = m.invoke(s, new Object[]{new Integer(0), new Integer(4)});
//        System.out.println(obj);



        JspTag parent = null;
        Class c=null;

        SimpleTag parent2=(SimpleTag) getParent();
        while(parent2!=null){
            parent2=(SimpleTag) parent2.getParent();
            nestedCount++;
        }

        for(int i=0;i<nestedCount;i++){
            parent=getParent();
            c = parent.getClass();
        }
        //getJspContext().getOut().print(nestedCount);

        //setLike(this);
//        getLike().setIgnoreCase(this.isIgnoreCase());
//        getLike().setLikeValue(this.getLikeValue());
//        getLike().setMatchMode(this.getMatchMode());
//        getLike().setPropertyName(this.getPropertyName());

        Object obj = null;
        //String s = null;


        try {
//            Class[] t = new Class[1];
//            t[0] = my.Employee.class;
            Method m1 = c.getMethod("setLike", new Class[]{Like.class});
//            Method m2 = c.getMethod("setLikeValue", new Class[]{String.class});
//            Method m3 = c.getMethod("setMatchMode", new Class[]{String.class});
//            Method m4 = c.getMethod("setIgnoreCase", new Class[]{boolean.class});
            try {
                obj = m1.invoke(parent, this);
//                obj = m2.invoke(parent, new String(getLikeValue()));
//                obj = m3.invoke(parent, new String(getMatchMode()));
//                obj = m4.invoke(parent, new Boolean(isIgnoreCase()));
                //getJspContext().getOut().print(o2);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        }




    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the matchMode
     */
    public String getMatchMode() {
        return matchMode;
    }

    /**
     * @param matchMode the matchMode to set
     */
    public void setMatchMode(String matchMode) {
        this.matchMode = matchMode;
    }

    /**
     * @return the likeValue
     */
    public String getLikeValue() {
        return likeValue;
    }

    /**
     * @param likeValue the likeValue to set
     */
    public void setLikeValue(String likeValue) {
        this.likeValue = likeValue;
    }

    /**
     * @return the ignoreCase
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase the ignoreCase to set
     */
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

//    /**
//     * @return the like
//     */
//    public Like getLike() {
//        return like;
//    }
//
//    /**
//     * @param like the like to set
//     */
//    public void setLike(Like like) {
//        this.like = like;
//    }

    

    
}
