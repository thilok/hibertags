/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package taghandlers.restrictions;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;

/**
 *
 * @author Thilok
 */
public class Eq extends SimpleTagSupport {

    private String propertyName;
    private String value;
    private String type;
    private boolean ignoreCase;
    int nestedCount = 0;

    public void doTag() throws JspException, IOException {
        JspTag parent = null;
        Class c = null;
        SimpleTag parent2 = (SimpleTag) getParent();
        while (parent2 != null) {
            parent2 = (SimpleTag) parent2.getParent();
            nestedCount++;
        }
        for (int i = 0; i < nestedCount; i++) {
            parent = getParent();
            c = parent.getClass();
        }
        Object obj = null;
        try {
            Method m1 = c.getMethod("setEq", new Class[]{Eq.class});
            try {
                obj = m1.invoke(parent, this);
            } catch (IllegalAccessException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IllegalArgumentException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvocationTargetException ex) {
                Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (NoSuchMethodException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SecurityException ex) {
            Logger.getLogger(Like.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the propertyName
     */
    public String getPropertyName() {
        return propertyName;
    }

    /**
     * @param propertyName the propertyName to set
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return the ignoreCase
     */
    public boolean isIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase the ignoreCase to set
     */
    public void setIgnoreCase(boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
}
